# Introduction
This receipe is split into two parts. This is due to the cook being in two parts; the first being the initial flavour extraction from the mirepoix, and the second, allowing for fresh vegetables to be added creating a better overall layered flavour profile.

# Ingredients: 
## Stage 1:
- 500ml Chicken Stock
- 30g Gelatin
- ~30ml Vegetable Oil
- 1kg Braising Steak (dry brined if possible), aim towards a thicker cut (i.e., ask the butcher or buy a large piece of meat and cut it yourself) -- aim for three cuts of 5-7cm in thickness.

> We're going to brown the meat, critical for developing layers of flavour. 

- 1 whole yellow onion
- 1 whole carrot
- 3 celery
- 4 Garlic Gloves
- 750ml Dry Red Wine
- 15ml Fish Sauce
- 15ml Soy Sauce
- 1 Bag Bouquet Garni
    - Sprigs of Thyme
    - Parsley
    - Bay Leaves
- All-purpose Flour

## Stage 2:
- 100g Pancetta
- Silverskin / Pearl Onions
- Chanteney Carrots
- Small mushrooms

# Mise en place:

## Stage 1:
1. Add chicken stock to a container, and then add gelatin. Let it absorb. 
2. If you have not dry brined the beef, pat it with a paper towel, and then season the beef with salt.
3. Chop the carrot length ways, so that you have 2 large pieces. Don't worry about peeling it. 
4. Chop the onion in half. Don't worry about the skin.
5. Chop the celery in half length ways.
5. Crush the garlic with the side of a large knife, and remove the skin.
6. Get the bouquet garni, and using some butchers twine, tie it together with the bay leaves, thyme and parsley, creating a twig-like bundle.

## Stage 2:
1. Remove ends of chanteney carrots, wash and if necessary peel. Prep silverskin/pearl onions. These can remain in the same appropriate vessel.

# Directions:

## Stage 1:
1. In a dutch oven over the boiling plate, sear the beef for around 3 (for a thinner cut) to 5 (for a thicker cut) minutes on each side. Remove the seared beef from the dutch oven, and then chop it into quite large chunks as the beef will shrink as it cooks. Coat the beef with flour (thickening agent). 
2. Set the dutch oven over the simmering plate. Add the carrot, onion and celery and let it brown for 3-5 minutes. Then, add the garlic and let it brown for another 2-3 minutes at most.

> We do not want to brown the garlic for too long, as it can yield a quite unpleasent bitter taste.

3. Using some red wine, first deglaze the dutch oven. Then, add the remainder of the red wine, along with the stock and gelatin mix.
4. Add the bouquet garni bundle, the soy sauce, and the fish sauce. 
5. Add the beef back into the dutch oven. Ensure all the juices also make it into the dutch oven.
5. Move the dutch oven to the roasting oven, and set a timer for 45 minutes. Ensure that you have a crack in the lid of the dutch oven for evaporative cooling to take place.
6. Once the timer chimes, use a pan to brown the pancetta on the simmering plate for around 7 minutes. Once cooked, store in an appropriate vessel.
7. Cook the mushrooms in the same pan for around 10 minutes.

> You might need a little more oil but generally the oil from the pancetta should be enough.

8. Add chanteney carrots, and pearl/silverskin onions and sautée for a further 5 minutes. 
9. Add onion, mushroom and pancetta to the stew. Set a timer for 60 minutes. 
10. Once timer chimes, remove stew. Season with salt and pepper to taste. Allow to cool for 10 minutes.
11. Using a paper towel, skim off some of the fat from the suface of the stew. 
12. Serve on a bed of mash, and garnish with some parsley.

# Timeline

This meal should be prepared around 2hr30min in advance. The meal is very easy to prepare and cook, but does take a little longer than most meals and you do need to be present at least intermittently. 

- Sear the beef first. This will take 6-10 minutes.
- Mise en place stage 1 should take around 5-10 minutes.
- Total active cooking time (including searing / browning vegetables) should be around 15 minutes.
- First stage cook is 1 hour. After 45 minutes, you'll start stage 2 mise en place and cooking. 
- Second stage cook is another hour.
